# CI Bricks

Portable and modular CI/CD. 
Create your bash script executed in your specified container on any platform (gitlab/github/jenkins/...)
This projects aims at providing commands that can be used by any projects, simplifying and consolidating CI/CD.

## Limitations
As this is a new project there are few things that need to be done:
- filter command per branch

## How it works
The orchestrator image is build and the orchestrator is executed in this image. It is going to look for all the project(s) `ci.json` file. It will then build the docker container for each project, running all the commands specified in the `ci.json` in this container. 

### Requirements
1. Your project ci/cd must be configured to clone this repository and execute the [execute_pipeline.sh](./docs/execute_pipeline.md) script.  
Example:
```yml
    - git submodule add -f ../global-ci-cd.git ./scripts/ci-cd
    - git submodule sync --recursive
    - git submodule update --init --recursive
    - ./scripts/ci-cd/.docker/development_start.sh --no-tty --command "./scripts/ci-cd/execute_pipeline.sh --no-tty --projects-root-dir ./packages" --ci-env-vars "BIT_,ECR_,NOW_,NPM_"
```
2. Your project(s in case of monorepos) must have a `ci.json` file listing the commands to be run in the docker container of your project
3. Your project(s) must have a `.docker` folder with the following structure  
  - .docker/  
        - dockefile  
        - docker-compose.yml  
        - .env.template

4. Your dockerfile must have all the tools needed to execute the required commands.

## ci.json file
This file defines what command to execute.  
All commands will be executed in the same running container so all temporary files will be kept between commands.  

Example
```json
{
    "commands": [
        ["bash", "BIT_TOKEN=${BIT_TOKEN} && yarn install"],
        ["bash", "yarn test"],
        ["bash", "yarn build"],
        ["vercel", "--project-id ${NOW_FRONTEND_PROJECT_ID} --org-id ${NOW_FRONTEND_ORG_ID} --token ${NOW_TOKEN}"],
        ["mr_note", "--token ${CI_JOB_TOKEN}"],
        ["release", "--token ${CI_JOB_TOKEN}"],
        ["ffm_deploy", "--out-dir dist --name billing --ffm-url https://staging-api.[MASKED].com/ffm"]
    ]
}
```

## Environment Variables
By default all the `DEBUG*`, `CI*`, `GITLAB*`, `HOST_PATH*` environment variables are passed down to the orchestrator. They can then be used by your command parameter(s) to pass them down to the commands.

If you want to pass down custom environment variables, that you have defined in the CI tool, to the orchestrator you will need to call the `execute_pipeline.sh` with the `--ci-env-vars` parameter as explained in the [execute_pipeline documentation](./docs/execute_pipeline.md)  

e.g from above `ci.json`  
```json
["vercel", "--project-id ${NOW_FRONTEND_PROJECT_ID} --org-id ${NOW_FRONTEND_ORG_ID} --token ${NOW_TOKEN}"],
```

## Commands

* [bash](./docs/bash.md)
* [docker_build_image](./docs/docker_build_image.md)
* [docker_login](./docs/docker_login.md)

