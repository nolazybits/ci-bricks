ARG DEBIAN_VERSION

FROM debian:${DEBIAN_VERSION}
ARG PROJECT_ID
ENV DEBIAN_FRONTEND noninteractive
ENV PROJECT_ID=${PROJECT_ID}

USER root

# Update all packages
RUN apt-get clean \
    && apt-get -y -q update \
    && apt-get -y -q install \
        acl \
        apt-transport-https \
        ca-certificates \
        curl \
        git \
        gnupg-agent \
        jq \
        locales-all \
        procps \
        python3 \ 
        python3-pip \
        screen \
        software-properties-common \
        vim \
        zsh

RUN pip3 install yq

# install docker-compose
RUN curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose \
    && chmod +x /usr/local/bin/docker-compose

# install docker-ce and docker-ce-cli
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - \
    && add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" \
    && apt-get -y -q update \
    && apt-get -y -q install docker-ce-cli

# Create user for our app and add it to the docker group
RUN useradd --user-group --create-home --shell /bin/false ${PROJECT_ID} 

# make sure the docker.sock exists and is having the proper permissions
RUN groupadd docker \
    && usermod -aG docker ${PROJECT_ID} \
    && newgrp docker 

# && setfacl -m user:${PROJECT_ID}:rw /var/run/docker.sock
# RUN touch /var/run/docker.sock \
#     && chown :1000 /run \
#     && chown -h 1000:root /var/run \
#     && chmod -R 775 /run \
#     && chmod g+s /run \
#     && chown 1000:root /var/run/docker.sock

# set our home
ENV HOME=/home/${PROJECT_ID}

# change the history location so we can mount it
RUN mkdir -p ${HOME}/docker
ENV HISTFILE=${HOME}/docker/.bash_history

# switch to non root user
USER ${PROJECT_ID}

# add profile to bash in case dev uses bash
RUN echo "PS1='${PROJECT_ID}:\w$ '" >> ~/.profile \
    && echo ". ~/.profile" > ~/.bashrc

# add zgen
RUN git clone https://github.com/tarjoilija/zgen.git "${HOME}/.zgen"

# create zshrc
ENV ZSH_DISABLE_COMPFIX="true"
RUN printf "\n\
    export TERM=\"xterm-256color\" \n\
    export LANG=\"en_US.UTF-8\" \n\
    export LC_COLLATE=\"en_US.UTF-8\" \n\
    export LC_CTYPE=\"en_US.UTF-8\" \n\
    export LC_MESSAGES=\"en_US.UTF-8\" \n\
    export LC_MONETARY=\"en_US.UTF-8\" \n\
    export LC_NUMERIC=\"en_US.UTF-8\" \n\
    export LC_TIME=\"en_US.UTF-8\" \n\
    export LC_ALL=\"en_US.UTF-8\" \n\
    export PATH=~/.local/bin:\${PATH} \n\
    export PATH=/usr/local/sbin:\${PATH} \n\
    export PATH=~/application/node_modules/.bin:\${PATH} \n\
    fpath=(~/.zsh_completion \"\${fpath[@]}\") \n\
\n\
    # PowerLevel Config \n\
    #POWERLEVEL9K_MODE='awesome-patched' \n\
    POWERLEVEL9K_MODE='flat' \n\
    POWERLEVEL9K_SHORTEN_DIR_LENGTH=2 \n\
    POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(os_icon dir vcs) \n\
    POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status) \n\
\n\
    POWERLEVEL9K_OS_ICON_BACKGROUND=\"white\" \n\
    POWERLEVEL9K_OS_ICON_FOREGROUND=\"blue\" \n\
    POWERLEVEL9K_DIR_HOME_FOREGROUND=\"white\" \n\
    POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND=\"white\" \n\
    POWERLEVEL9K_DIR_DEFAULT_FOREGROUND=\"white\" \n\
\n\
# Source Profile \n\
[[ -e ~/.profile ]] && emulate sh -c 'source ~/.profile' \n\
\n\
# Load compinit for autocompletion \n\
# autoload -Uz compinit && compinit -i \n\
\n\
# finally load zgen \n\
source \"\${HOME}/.zgen/zgen.zsh\" \n\
\n\
# if the init scipt doesn't exist \n\
if ! zgen saved; then \n\
    echo \"Creating a zgen save\" \n\
    # Load the oh-my-zsh's library. \n\
    zgen oh-my-zsh \n\
\n\
    # plugins \n\
    zgen oh-my-zsh plugins/git \n\
    zgen oh-my-zsh plugins/sudo \n\
    zgen oh-my-zsh plugins/command-not-found \n\
    zgen load zsh-users/zsh-syntax-highlighting \n\
\n\
    # completions \n\
    zgen load zsh-users/zsh-completions src \n\
\n\
    # theme \n\
    # zgen load bhilburn/powerlevel9k powerlevel9k \n\
\n\
    # save all to init script \n\
    zgen save \n\
fi \n\
setopt PROMPT_SUBST \n\
PROMPT='%%~ $ ' \n " > ~/.zshrc

# setting zsh as default screen
RUN printf "\
    shell \"/usr/bin/zsh\" \n\
" > ~/.screenrc

USER root
RUN chown -R ${PROJECT_ID}:${PROJECT_ID} "${HOME}" && chmod -R 755 "${HOME}" 
# switch to this user | removed for now as can't seem to connect to docker.sock otherwise
USER ${PROJECT_ID}

# set the working directory to be
WORKDIR "${HOME}/${APPLICATION_FOLDER}"