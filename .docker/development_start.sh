#!/bin/bash
set -e
if [ -n "${DEBUG}" ]; then
    set -x
fi

# assign SCRIPT_DIR and ROOT_DIR
if [[ "$OSTYPE" == "darwin"* ]]; then
    # MacOS
    echo "MacOS detected"
    export SCRIPT_DIR=$(cd "$(dirname "$0")"; pwd)
else
    # Linux
    echo "Linux OS detected"
    export SCRIPT_DIR=$(dirname $(readlink -f $0))
fi
export ROOT_DIR=$(realpath "${SCRIPT_DIR}/..")

# check if we have an .env file for docker-compose, if not create it from the template
if [[ ! -f "${SCRIPT_DIR}/.env" ]]; then
    cp ${SCRIPT_DIR}/.env.template ${SCRIPT_DIR}/.env
fi

# the host folder mounted. runners should provide this path
if [ -z "${HOST_PATH}" ]; then
    export HOST_PATH=${ROOT_DIR}
fi

# create the bash history file if not exists
if [ ! -f "${HOST_PATH}/.docker/.persist/.bash_history" ]; then
    mkdir -p "${HOST_PATH}/.docker/.persist/.bash_history"
fi

CI_ENV_VARS=""
function generate_inline_env
{
    # add special env var to our .env, this is mainly to pass CI env var
    INLINE_ENV=""
    ENV_VARS=$( env | awk -F "=" '{print $1}' | grep -E "^(DOCKER_CLI_EXPERIMENTAL|DEBUG|CI|GITLAB|HOST_PATH${CI_ENV_VARS})" )

    for ENV_VAR in ${ENV_VARS[@]}
    do
        INLINE_ENV="$INLINE_ENV --env ${ENV_VAR}=$( printf "%q" ${!ENV_VAR} )"
    done

    INLINE_ENV="$INLINE_ENV --env CI_ENV_VARS=$( printf "%q" ${CI_ENV_VARS} )"
    # if [[ "$OSTYPE" != "darwin"* ]]; then
    #     # Linux
    #     if [[ -n "$INLINE_ENV" ]]; then
    #         INLINE_ENV="-e $INLINE_ENV"
    #     fi
    # fi
    
    echo "${INLINE_ENV}"
}


echo "--- Starting Ochestrator with the following env vars ---"
env
echo "----------------"

BUILD=""
COMMANDS=()
TTY="-it"

while [ $# -gt 0 ] 
do
    key="$1"
    case $key in
        --build)
            BUILD="true"
            shift # past argument
        ;;
        --no-tty)
            TTY=" "
            shift
        ;;
        -c|--command)
            shift # past argument
            SHELL_COMMAND=$1
            shift # past argument
        ;;
        --ci-env-vars)
            shift # past argument
            export CI_ENV_VARS="|${1//,/|}"
            shift # past argument
        ;;
        *)    # unknown option
            echo "Invalid option $1"
            exit 1;
        ;;
    esac
done

cd "$SCRIPT_DIR"
if [[ -n ${BUILD} ]]; then
    docker-compose build --no-cache
fi

# up the container
docker-compose up -d

# change the permission of the docker.sock
docker exec ${TTY} -u 0 cicd-development bash -c "
    chmod 666 /var/run/docker.sock
    chown root:docker /var/run/docker.sock
"

# run the commmand
# if we have no command (like for instance passed by the CI) just run zsh
if [ -z "${SHELL_COMMAND}" ]; then
    echo "No command passed";
    COMMANDS+=("docker exec $(generate_inline_env) ${TTY} cicd-development zsh")
# otherwise execute the command
else
    # add the up and exec
    COMMANDS+=("docker exec $(generate_inline_env) ${TTY} cicd-development zsh -c \"${SHELL_COMMAND}\"")
fi

# join the commands in a string and execute
COMMAND_STRING=$(printf " && %s" "${COMMANDS[@]}")
COMMAND_STRING=${COMMAND_STRING:3}

if [ -n "${DEBUG}" ]; then
    echo "${COMMAND_STRING}";
fi

eval "${COMMAND_STRING}"
