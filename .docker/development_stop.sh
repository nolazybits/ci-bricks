#!/bin/bash

# assign SCRIPT_DIR and ROOT_DIR
if [[ "$OSTYPE" == "darwin"* ]]; then
    # MacOS
    echo "MacOS detected"
    export SCRIPT_DIR=$(cd "$(dirname "$0")"; pwd)
else
    # Linux
    echo "Linux OS detected"
    export SCRIPT_DIR=$(dirname $(readlink -f $0))
fi
export ROOT_DIR=$(realpath "${SCRIPT_DIR}/..")

cd "$SCRIPT_DIR"
HOST_PATH="${ROOT_DIR}" docker-compose down