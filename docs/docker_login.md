# docker_login
> Login to a docker registry


## Parameters
**--ecr-registry**  
The registry to log in to.    

**--ecr-username**  
The username to use to log in.    

**--ecr-password**  
The password to use to log in.  

**--aws-login**  
if this flag exists aws-login will be used instead  

**--aws-region**  
if aws-login is used, then defines which region to log in to

## Examples

<hr>

```json
["docker_login", "--ecr-registry ${ECR_REGISTRY} --aws-login --aws-region ap-southeast-2"]
```
will log the user in using aws-login for the region ap-southeast-2  