# docker_build_image
> Build a docker image and push it to a registry


## Parameters
**--ecr-registry**  
The registry to push the image to.    
Note you will have to be logged in already, using the [docker_login](./docker_login.md) command

**--ecr-repository**  
The registry to push the image to.    

**--ecr-repository-production**  
The registry to push the image to when on master.    

**--image-tag**  
The image tag to use.  

_defaults_:  
* on master: `${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHA}-${CI_PIPELINE_ID}`  
* on other branches: `${CI_COMMIT_REF_SLUG}`

**--build-arg**  
Any specific build arguments to send to docker build when building the dockerfile

**--docker-file**  
The location of the docker file to build, relative to the project root

**--target**  
The target to build (in case of multi stage dockerfile)

**--target-production**:  
The target to build (in case of multi stage dockerfile) when running on master branch

## Examples

<hr>

```json
["docker_build_image", "--docker-file .docker/dockerfile --ecr-registry ${ECR_REGISTRY} --ecr-repository ${ECR_REPOSITORY}-base --ecr-repository-production ${ECR_REPOSITORY} --build-arg NODE_VERSION=${NODE_VERSION} --build-arg DEBIAN_VERSION=${DEBIAN_VERSION} --target development --target-production production"]
```
will build the development target or production target if run on any branch or master.  