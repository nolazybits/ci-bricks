# execute_pipeline
> Execute our CI/CD pipeline

This is executed from within the orchestrator image so starting it looks like this
```yml
./scripts/ci-cd/.docker/development_start.sh --no-tty --command "./scripts/ci-cd/execute_pipeline.sh ..."
```

## Parameters
**--projects-root-dir**  
the root directory to start scanning projects from

**--no-tty**  
Run docker without tty

## Examples

<hr>

```yml
./scripts/ci-cd/.docker/development_start.sh --no-tty --command "./scripts/ci-cd/execute_pipeline.sh --no-tty --projects-root-dir ./packages" --ci-env-vars "BIT_,ECR_,NOW_,NPM_"
```
will start the orchestrator environment and run it.  
TODO: maybe abstract this