# bash
> Run any command in a bash subshell


## Parameters
**$@**: the command to run in the subshell.

## Examples

<hr>

```json
["bash", "echo \"It works\""]
```
echo `$ It Works`

<hr>

```json
["bash", "yarn install"]
```
runs the installation of node dependencies

<hr>

```json
["bash", "yarn test:ci"]
```
runs npm `test:ci` script

<hr>