#!/bin/bash
set -e
if [ -n "${DEBUG}" ]; then
    set -x
fi


while [ $# -gt 0 ] 
do
    key="$1"
    case $key in
        --project-id)
            PROJECT_ID=$2
            shift # past argument
            shift # past value
        ;;
        --org-id)
            ORG_ID=$2
            shift # past argument
            shift # past value
        ;;
        --token)
            TOKEN=$2
            shift # past argument
            shift # past value
        ;;
        *) # unknown option
            echo "Invalid option $1"
            exit 1;
        ;;
    esac
done

# check we have all variables set otherwise throw an error
for var in PROJECT_ID ORG_ID TOKEN
do
    if [[ -z "${!var:-}" ]]; then
        # to lower case  
        var=${var,,}
        # replace - by _
        var=${var//_/-}
        echo "--${var} has not been set and is required"
        exit 1
    else
        echo "${var}=${!var}"
    fi
done

echo "###############################################################"
echo "VERCEL DEPLOY"

if [ "${CI_COMMIT_BRANCH}" == "master" ]; then
    URL=$(VERCEL_PROJECT_ID=${PROJECT_ID} VERCEL_ORG_ID=${ORG_ID} npx vercel --prod --token="${TOKEN}" -c -f)
else
    URL=$(VERCEL_PROJECT_ID=${PROJECT_ID} VERCEL_ORG_ID=${ORG_ID}  npx vercel --token="${TOKEN}" -c -f)
fi

VERCEL_URL=$(echo "${URL}" | sed s/'http:\/\/'/''/g | sed s/'https:\/\/'//g)

# scripts are run in their own subshell pass 
echo "export FRONTEND_PREVIEW_URL=${VERCEL_URL}" >> variables-frontend-url # refer to https://gitlab.com/gitlab-org/gitlab-runner/issues/1099
echo "See preview on ${VERCEL_URL}"

echo "VERCEL DEPLOY [DONE]"
echo "###############################################################"
