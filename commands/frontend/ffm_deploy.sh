#!/bin/bash

set -e
if [ -n "${DEBUG}" ]; then
    set -x
fi

RELEASE="false"
while [ $# -gt 0 ] 
do
    key="$1"
    case $key in
        --name)
            NAME=$2
            shift # past argument
            shift # past value
        ;;
        --out-dir)
            OUTPUT_DIR=$2
            shift # past argument
            shift # past value
        ;;
        --branch-staging)
          BRANCH_STAGING=$2
          shift
          shift
        ;;
        --ffm-url-staging)
            FFM_URL_STAGING=$2
            shift # past argument
            shift # past value
        ;;
        --ffm-url-production)
            FFM_URL_PRODUCTION=$2
            shift # past argument
            shift # past value
        ;;
        --release)
            RELEASE=$2
            shift # past argument
            shift # past value
        ;;
        *) # unknown option
            echo "Invalid option $1"
            exit 1;
        ;;
    esac
done


# check we have all variables set otherwise throw an error
for var in NAME OUTPUT_DIR FFM_URL_STAGING FFM_URL_PRODUCTION BRANCH_STAGING
do
    if [[ -z "${!var:-}" ]]; then
        # to lower case  
        var=${var,,}
        # replace - by _
        var=${var//_/-}
        echo "--${var} has not been set and is required"
        exit 1
    else
        echo "${var}=${!var}"
    fi
done

echo "###############################################################"
echo "FFM DEPLOY"

if [ "${CI_COMMIT_BRANCH}" == "${BRANCH_STAGING}" ]  || [ "${RELEASE}" == "true" ] ; then

  if [ "${RELEASE}" == "true" ]; then
    FFM_URL="${FFM_URL_PRODUCTION}"
  else
    FFM_URL="${FFM_URL_STAGING}"
  fi

  # loop in the dist folder and add this files to the bundles array (exclude html files)
  CURL_FILES=""
  PARENT_DIR=$(pwd)

  for filename in "${OUTPUT_DIR}"/*.*
  do
    if [ ! -e "${filename}" ] || [ "${filename: -5}" = ".html" ];
    then
      continue
    fi 

    echo "Selected build file: ${PARENT_DIR}/${filename}"
    CURL_FILES="${CURL_FILES} --form files=@${PARENT_DIR}/${filename}"
  done

  # add name / version /description or take this from template config that we need to create
  NAME=${NAME}
  VERSION=$(cat package.json | jq --raw-output '.version')
  DESCRIPTION=$(cat package.json | jq --raw-output '.description')
  CONFIG=$(cat ../../plugin_config.json)

# upload to FFM
generate_data()
{
cat <<EOF
{
  "name": "${NAME}",
  "version": "${VERSION}",
  "description": "${DESCRIPTION}",
  "plugin_config": ${CONFIG},
  "ou_feature_config_template": {},
  "is_default": true
}
EOF
}

  curl -v -i \
    --url ${FFM_URL}/features \
    --header "authorization: Bearer undefined" \
    --header 'Content-Type: multipart/form-data' \
    --header 'content-type: multipart/form-data; boundary=---011000010111000001101001' \
    $CURL_FILES \
    -F "body=$(generate_data);type=application/json"
  fi

echo "FFM DEPLOY [DONE]"
echo "###############################################################"  