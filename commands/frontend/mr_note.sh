#!/bin/bash

set -e
if [ -n "${DEBUG}" ]; then
    set -x
fi

while [ $# -gt 0 ] 
do
    key="$1"
    case $key in
        --token)
            TOKEN=$2
            shift # past argument
            shift # past value
        ;;
        *) # unknown option
            echo "Invalid option $1"
            exit 1;
        ;;
    esac
done

# check we have all variables set otherwise throw an error
for var in TOKEN
do
    if [[ -z "${!var:-}" ]]; then
        # to lower case  
        var=${var,,}
        # replace - by _
        var=${var//_/-}
        echo "--${var} has not been set and is required"
        exit 1
    else
        echo "${var}=${!var}"
    fi
done

if [ "${CI_COMMIT_BRANCH}" == "master" ]; then
    echo "###############################################################"
    echo "MR NOTE"
    . variables-frontend-url

    # url invoked for adding comment
    URL="${CI_SERVER_URL}/api/v4/projects/${CI_PROJECT_ID}/repository/commits/${CI_COMMIT_SHA}/comments"

    # create note body as multiline string (in markdown!)
    NOTE="\
    Deployed the commit [$CI_COMMIT_SHORT_SHA]($CI_PROJECT_URL/-/commit/$CI_COMMIT_SHA) to the following URLs:\
    - **Frontend**: [$FRONTEND_PREVIEW_URL](https://$FRONTEND_PREVIEW_URL)\
    "

    # POST request that creates message under commit
    curl --request POST -H "PRIVATE-TOKEN: ${TOKEN}" --form "note=${NOTE}" -k ${URL}

    echo "MR NOTE [DONE]"
    echo "###############################################################"
fi
