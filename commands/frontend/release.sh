#!/bin/bash

set -e
if [ -n "${DEBUG}" ]; then
    set -x
fi

while [ $# -gt 0 ] 
do
    key="$1"
    case $key in
        --token)
            TOKEN=$2
            shift # past argument
            shift # past value
        ;;
        --username)
            USERNAME=$2
            shift # past argument
            shift # past value
        ;;
        --email)
            EMAIL=$2
            shift # past argument
            shift # past value
        ;;
        *) # unknown option
            echo "Invalid option $1"
            exit 1;
        ;;
    esac
done

# check we have all variables set otherwise throw an error
for var in TOKEN USERNAME EMAIL
do
    if [[ -z "${!var:-}" ]]; then
        # to lower case  
        var=${var,,}
        # replace - by _
        var=${var//_/-}
        echo "--${var} has not been set and is required"
        exit 1
    else
        echo "${var}=${!var}"
    fi
done

echo "###############################################################"
echo "GITLAB RELEASE"

# Preconfigure GITLAB
git remote set-url origin https://$GITLAB_USER_NAME:$GITLAB_TOKEN@gitlab.internal.equipmentshare.com/$CI_PROJECT_PATH.git
git config user.email "${EMAIL}"
git config user.name "${USERNAME}"

# Fix for GITLAB issue: https://github.com/release-it/release-it/blob/master/docs/ci.md#error-tag-already-exists
git pull origin $CI_BUILD_REF_NAME

if [ "${CI_COMMIT_BRANCH}" == "master" ]; then
    export GITLAB_TOKEN=${TOKEN} && npx release-it -V --ci
else
    export GITLAB_TOKEN=${TOKEN} && npx release-it -V -d --ci
fi

chmod -f 777 ./CHANGELOG.md || :

echo "GITLAB RELEASE"
echo "###############################################################"
