#!/bin/bash
set -e
if [ -n "${DEBUG}" ]; then
    set -x
    echo "Running bash.sh with env"
    echo "$(env)"
fi

eval $@