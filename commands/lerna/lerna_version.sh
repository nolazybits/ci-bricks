#!/bin/bash

set -e

if [ -n "${DEBUG}" ]; then
    set -x
fi

PUSH_TO_GIT=false
RELEASE="false"

# required ENV VAR
# check we have all variables set otherwise throw an error
while [ $# -gt 0 ] 
do
    key="$1"
    case $key in
        --push)
            PUSH_TO_GIT=true
        ;;
        --branch-staging)
            BRANCH_STAGING=$2
            shift # past argument
            shift # past value
        ;;
        --release)
            RELEASE=$2
            shift # past argument
            shift # past value
        ;;
        *) # unknown option
            echo "Invalid option $1"
            exit 1;
        ;;
    esac
done

for var in BRANCH_STAGING
do
    if [[ -z "${!var:-}" ]]; then
        # to lower case  
        var=${var,,}
        # replace - by _
        var=${var//_/-}
        echo "--${var} has not been set and is required"
        exit 1
    else
        echo "${var}=${!var}"
    fi
done

git config --global user.email "${GITLAB_USER_EMAIL}"
git config --global user.name "${GITLAB_USER_NAME}"

# check if we are in CI or not
if [ "${CI}" != "true" ]; then
    CI_COMMIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
fi

# we are on the release branch bump the version normally
if [ "${RELEASE}" == "true" ]; then
    # create the versions using lerna
    npx lerna version --conventional-commits --conventional-graduate --no-push --yes
# we are on any other branch, create a rc version
elif [ "${CI_COMMIT_BRANCH}" == "${BRANCH_STAGING}" ]; then
    # create the versions using lerna
    npx lerna version --conventional-commits --conventional-prerelease --no-changelog --no-push --yes
fi

# push the new version and tags
if [ "${PUSH_TO_GIT}" == true ]; then
    git push origin "${CI_COMMIT_BRANCH}" --tags
fi