#!/bin/bash
set -e
if [ -n "${DEBUG}" ]; then
    set -x
fi

if [ "${CI_COMMIT_BRANCH}" == "master" ]; then
    echo "###############################################################"
    echo "FRONTEND - RELEASE"
    cd "../../"

    # Set global profile
    echo "GITLAB_USER_NAME: ${GITLAB_USER_NAME}"
    echo "GITLAB_USER_EMAIL: ${GITLAB_USER_EMAIL}"
    git config --global user.email "${GITLAB_USER_EMAIL}"
    git config --global user.name "${GITLAB_USER_NAME}"
    git reset --hard
    git checkout master

    echo "Bump versions of changed packages"
    GL_TOKEN=$GITLAB_TOKEN npx lerna version --conventional-commits --create-release gitlab --yes

    echo "FRONTEND - RELEASE [DONE]"
    echo "###############################################################"
fi