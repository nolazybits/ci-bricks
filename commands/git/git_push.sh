#!/bin/bash

set -e

if [ -n "${DEBUG}" ]; then
    set -x
fi

SKIP_CI="-o ci.skip"

while [ $# -gt 0 ] 
do
    key="$1"
    case $key in
        --username)
            USERNAME=$2
            shift
            shift
        ;;
        --token)
            TOKEN=$2
            shift
            shift
        ;;
        --no-skip-ci)
            SKIP_CI=""
        ;;
        *) # unknown option
            echo "Invalid option $1"
            exit 1;
        ;;
    esac
done

for var in USERNAME TOKEN
do
    if [[ -z "${!var:-}" ]]; then
        # to lower case  
        var=${var,,}
        # replace - by _
        var=${var//_/-}
        echo "--${var} has not been set and is required"
        exit 1
    else
        echo "${var}=${!var}"
    fi
done


# check if we are in CI or not
if [ "${CI}" != "true" ]; then
    CI_COMMIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
fi

# get the url of the repo and insert the username and token
GIT_REMOTE_URL=$(git config --get remote.origin.url)
GIT_REMOTE_URL=$(echo "${GIT_REMOTE_URL}" | sed -E "s/(http.*\/\/)(.*:.*@)?(.*)/\1${USERNAME}:${TOKEN}@\3/")
git push "${GIT_REMOTE_URL}" "${CI_COMMIT_BRANCH}" --tags ${SKIP_CI}