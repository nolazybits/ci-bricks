#!/bin/bash

# Required ENV VAR
# CI: Check if we are in a CI env
# CI_COMMIT_BRANCH: The branch this is being executed on
# CI_COMMIT_SHA: The branch this is being executed on
# CI_PIPELINE_ID
set -e
if [ -n "${DEBUG}" ]; then
    set -x
fi

echo "################################"
pwd
ls -la
echo "###############################"

function docker_tag_exists() 
{
    docker manifest inspect ${ECR_REGISTRY}/${ECR_REPOSITORY}:${IMAGE_TAG} > /dev/null ; return $?
}

BUILD_ARGS=""
TARGET=""
RELEASE="false"

while [ $# -gt 0 ] 
do
    key="$1"
    case $key in
        --ecr-registry)
            ECR_REGISTRY=$2
            shift # past argument
            shift # past value
        ;;
        --ecr-repository)
            ECR_REPOSITORY=$2
            shift # past argument
            shift # past value
        ;;
        --ecr-repository-production)
            ECR_REPOSITORY_PRODUCTION=$2
            shift # past argument
            shift # past value
        ;;
        --image-tag)
            IMAGE_TAG=$2
            shift
            shift
        ;;
        --build-arg)
            BUILD_ARGS="${BUILD_ARGS} --build-arg $2"
            shift # past argument
            shift # past value
        ;;
        --docker-file)
            DOCKER_FILE+=$2
            shift
            shift
        ;;
        --target)
            TARGET=$2
            shift
            shift
        ;;
        --target-production)
            TARGET_PRODUCTION=$2
            shift
            shift
        ;;
        --branch-staging)
            BRANCH_STAGING=$2
            shift
            shift
        ;;
        --release)
            RELEASE=$2
            shift # past argument
            shift # past value
        ;;
        --package-json)
            PACKAGE_JSON=$2
            shift
            shift
        ;;
        *) # unknown option
            echo "Invalid option $1"
            exit 1;
        ;;
    esac
done

if [ -z ${PACKAGE_JSON} ]; then
    PACKAGE_JSON="package.json"
fi
PACKAGE_JSON="$(realpath "${PACKAGE_JSON}")"

# check we have all variables set otherwise throw an error
for var in ECR_REGISTRY ECR_REPOSITORY ECR_REPOSITORY_PRODUCTION DOCKER_FILE TARGET TARGET_PRODUCTION BRANCH_STAGING
do
    if [[ -z "${!var:-}" ]]; then
        # to lower case  
        var=${var,,}
        # replace - by _
        var=${var//_/-}
        echo "--${var} has not been set and is required"
        exit 1
    else
        echo "${var}=${!var}"
    fi
done

# check if we are in CI or not
if [ "${CI}" != "true" ]; then
    CI_COMMIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
    CI_COMMIT_SHA=$(git rev-parse HEAD)
    CI_COMMIT_REF_SLUG=${CI_COMMIT_SHA}
fi

echo " On branch ${CI_COMMIT_BRANCH}"
echo ${PACKAGE_JSON}

if [ "${CI_COMMIT_BRANCH}" != "master" ]; then
    exit 0
fi

if [ -f ${PACKAGE_JSON} ]; then
    VERSION=$(jq -r '.version' ${PACKAGE_JSON})
fi
echo "VERSION RELEASE: ${VERSION}"

# we want to deploy to prod
if [ "${RELEASE}" == "true" ]; then
    # check if we are running this in gitlab or localy
    if [ "${CI}" == "true" ]; then
        echo "In CI"
        TARGET=${TARGET_PRODUCTION}
        ECR_REPOSITORY=${ECR_REPOSITORY_PRODUCTION}
    fi
    docker_tag_exists 2>/dev/null && exit 0
    IMAGE_TAG="${VERSION}-${CI_COMMIT_SHA}"
# if we have push to the default branch, create automatically an image tag
elif [ "${CI_COMMIT_BRANCH}" == "${BRANCH_STAGING}" ]; then
    IMAGE_TAG="${VERSION}-${CI_COMMIT_SHA}"
# we are building a branch - not pre-release and not release
else
    # add the pipeline id
    IMAGE_TAG="${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHA}-${CI_PIPELINE_ID}"
fi
echo "###############################################################"
echo "DOCKER - BUILD IMAGE ${IMAGE_TAG}"
echo "Will be pushing target ${TARGET} to ${ECR_REPOSITORY}"
TARGET="--target ${TARGET}"

COMMAND="docker build -f ${DOCKER_FILE} ${TARGET}"
COMMAND="${COMMAND} ${BUILD_ARGS}"
COMMAND="${COMMAND} --pull"
COMMAND="${COMMAND} --cache-from $ECR_REGISTRY/$ECR_REPOSITORY:${IMAGE_TAG}"
COMMAND="${COMMAND} --tag ${ECR_REGISTRY}/${ECR_REPOSITORY}:${IMAGE_TAG}"
COMMAND="${COMMAND} $(pwd)"

# build the image
eval ${COMMAND}

# now push to the ECR
docker push ${ECR_REGISTRY}/${ECR_REPOSITORY}:${IMAGE_TAG}
echo "DOCKER - BUILD IMAGE ${IMAGE_TAG} [DONE]"
echo "###############################################################"
