#!/bin/bash
set -e
if [ -n "${DEBUG}" ]; then
    set -x
fi


while [ $# -gt 0 ] 
do
    key="$1"
    case $key in
        --ecr-registry)
            ECR_REGISTRY=$2
            shift # past argument
            shift # past value
        ;;
        --ecr-username)
            ECR_USERNAME="-u ${2}"
            shift # past argument
            shift # past value
        ;;
        --ecr-password)
            ECR_PASSWORD="-p ${2}"
            shift # past argument
            shift # past value
        ;;
        --aws-login)
            AWS_LOGIN="true"
            shift
        ;;
        --aws-region)
            AWS_REGION=$2
            shift # past argument
            shift # past value
        ;;
        *) # unknown option
            echo "Invalid option $1"
            exit 1;
        ;;
    esac
done

# check we have all variables set otherwise throw an error
for var in ECR_REGISTRY
do
    if [[ -z "${!var:-}" ]]; then
        # to lower case  
        var=${var,,}
        # replace - by _
        var=${var//_/-}
        echo "--${var} has not been set and is required"
        exit 1
    else
        echo "${var}=${!var}"
    fi
done

if [ "${AWS_LOGIN}" == "true" ]; then
    if [[ -z ${AWS_REGION} ]]; then
        echo "when using --aws-login you need to specify --aws-region"
        exit 1
    fi
    eval $(aws ecr get-login --no-include-email --region ${AWS_REGION})
else
    echo "Docker login to ${ECR_REGISTRY}"
    docker login ${ECR_USERNAME} ${ECR_PASSWORD} ${ECR_REGISTRY}
fi