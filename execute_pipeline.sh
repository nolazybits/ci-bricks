#!/bin/bash
# make sure this script exists if any of the subcommands fail
set -e
if [ -n "${DEBUG}" ]; then
    set -x
fi

# assign SCRIPT_DIR
if [[ "$OSTYPE" == "darwin"* ]]; then
    # MacOS
    echo "MacOS detected"
    SCRIPT_DIR=$(cd "$(dirname "$0")"; pwd)
else
    # Linux
    echo "Linux OS detected"
    SCRIPT_DIR=$(dirname $(readlink -f $0))
fi


if [ -z ${CI_COMMIT_BRANCH} ]; then
    CI_COMMIT_BRANCH=${CI_BUILD_REF_NAME}
fi

if [ "${CI}" == "true" ]; then
    echo "Checking out ${CI_BUILD_REF_NAME}"
    # checkout the branch to not be in a detached state
    git fetch -a --quiet
    git checkout ${CI_BUILD_REF_NAME}
    # change the mod to 777, leave this as otherwise trying to release to the checked out branch will fail due to permission error
    find .git/ -user ${PROJECT_ID} -exec chmod -R 777 {} +
fi

# make docker use experimental feature
export DOCKER_CLI_EXPERIMENTAL="enabled"

# find the mounting points of the cicd folder
CI_CD_FOLDER=$(docker inspect -f "{{ range .Mounts }}{{if eq .Name \"$(whoami)-data\"}}{{.Destination}}{{end}}{{end}}" $(whoami)-development)
HOST_ROOT_FOLDER=$(docker volume inspect -f '{{ .Options.device }}' $(whoami)-data)
HOST_CI_CD_FOLDER=$(readlink -m "${HOST_ROOT_FOLDER}/$(dirname "$0")")

# the host folder mounted. runners should provide this path
if [ -z "${HOST_PATH}" ]; then
    HOST_PATH=${HOST_ROOT_FOLDER}
fi

function docker_env_inline
{
    # add all the docker-compose env var
    local ENV_DOCKER_COMPOSE=$( echo $( grep -v -E '^(#.*|[[:space:]]*|.*=\s*)$' "${1}" ) )
    local ENV_HOSTS=$( echo $( set | grep -E "^(DOCKER_CLI_EXPERIMENTAL|DEBUG|CI|GITLAB|HOST_${CI_ENV_VARS})" | grep -v -E '^(#.*|[[:space:]]*|.*=\s*)$' ) )
    # add all the hosts custom env vars
    local MERGED_ENV_VARS="${ENV_DOCKER_COMPOSE} ${ENV_HOSTS}"

    echo ${MERGED_ENV_VARS}
}

function generate_inline_env
{
    # ENV_VARS=$( generate_env ${1} )
    # get all the env vars from the docker
    # INLINE_ENV=$( echo $( grep -v -E '^(#.*|[[:space:]]*|.*=\s*)$' "${1}" | while read line; do echo "-e $line"; done ) )

    # add special env var to our .env, this is mainly to pass CI env var
    local ENV_VARS=$( set | awk -F "=" '{print $1}' | grep -E "^(DOCKER_CLI_EXPERIMENTAL|DEBUG|CI|GITLAB|HOST_|PROJECT_${CI_ENV_VARS})" )
    local INLINE_ENV=""
    for ENV_VAR in ${ENV_VARS[@]}
    do
        INLINE_ENV="$INLINE_ENV -e ${ENV_VAR}=$( printf "%q" ${!ENV_VAR} )"
    done

    # if [[ "$OSTYPE" != "darwin"* ]]; then
    #     # Linux
    #     if [[ -n "$INLINE_ENV" ]]; then
    #         INLINE_ENV="-e $INLINE_ENV"
    #     fi
    # fi
    
    echo "${INLINE_ENV}"
}

function find_docker_compose
{
    local CI_JSON=$1
    local PROJECT_PATH=$(dirname ${CI_JSON})

    # find the dockercompose file 
    if [ $(jq -rc 'has("dockercompose")' ${CI_JSON}) == "true" ]; then
        DOCKER_COMPOSE_FILE=$(jq -rc '.dockercompose' ${CI_JSON})
    else
        DOCKER_COMPOSE_FILE="${PROJECT_PATH}/.docker/docker-compose.yml"
    fi

    if [  ! -f "${DOCKER_COMPOSE_FILE}" ]; then
        echo "Missing or wrong dockercompose property in ${PROJECT_PATH}/ci.json or missing ${DOCKER_COMPOSE_FILE} file"
        exit 1
    fi

    echo ${DOCKER_COMPOSE_FILE}
}

function run_project
{
    local CI_JSON=$1
    local CI_CD_CI_JSON=$(realpath "${CI_CD_FOLDER}/${CI_JSON}")
    local CI_CD_PROJECT_PATH="$(dirname ${CI_CD_CI_JSON})"
    local CHAIN=$2
    local COMMANDS_ARRAY
    local DOCKER_COMPOSE_FILE
    local DOCKER_COMPOSE_PATH

    if [ ! -z ${CHAIN} ]; then
        CHAIN=".${CHAIN}"
    fi

    # check this is a file as it also returns the pattern when nothing is found
    [ -f "$CI_JSON" ] || return 0
    # make sure we can access the file
    # chmod 755 ${CI_CD_CI_JSON} || true

    # COMMANDS=($(<${CI_JSON} jq -r '.commands | @sh'))
    # COMMANDS=$(jq '.commands' $CI_JSON)
    # COMMANDS=($((<${CI_JSON} jq -r '.commands | @sh') | tr -d \'\"))
    if [ "$(eval "jq -rc 'select(${CHAIN}.commands != null)' $CI_CD_CI_JSON")" != "" ]; then
        mapfile -t COMMANDS_ARRAY <<< $(eval "jq -rc '${CHAIN}.commands[]' $CI_CD_CI_JSON")
    fi
    [ -n "${COMMANDS_ARRAY}" ] || return 0
    
    WORKING_DIR=$(eval "jq -rc '.working_dir' ${CI_CD_CI_JSON}")
    if [[ -z "${WORKING_DIR}" || "${WORKING_DIR}" == "null" ]]; then
        WORKING_DIR=""
    else
        WORKING_DIR="--workdir=${WORKING_DIR}"
    fi

    echo "Processing ${CI_CD_CI_JSON} using ${WORKING_DIR:-"dockerfile working directory"} as working directory"

    # copy the docker compose and env file to one with ci extension
    DOCKER_COMPOSE_FILE="$(find_docker_compose ${CI_CD_CI_JSON})"
    DOCKER_COMPOSE_PATH="$(dirname ${DOCKER_COMPOSE_FILE})"
    cp "${DOCKER_COMPOSE_FILE}" "${DOCKER_COMPOSE_PATH}/docker-compose.ci.yml"
    cp "${DOCKER_COMPOSE_PATH}/.env.template" "${DOCKER_COMPOSE_PATH}/.env.ci"
    DOCKER_COMPOSE_FILE="${DOCKER_COMPOSE_PATH}/docker-compose.ci.yml" 
    DOCKER_COMPOSE_ENV_FILE="${DOCKER_COMPOSE_PATH}/.env.ci"   
    
    # path relative to host
    HOST_PROJECT_PATH=$(readlink -m "${HOST_ROOT_FOLDER}/$(dirname "${CI_JSON}")")
    HOST_DOCKER_COMPOSE_PATH=${DOCKER_COMPOSE_PATH/$CI_CD_PROJECT_PATH/$HOST_PROJECT_PATH}
    HOST_DOCKER_COMPOSE_FILE=${DOCKER_COMPOSE_FILE/$CI_CD_PROJECT_PATH/$HOST_PROJECT_PATH}
    HOST_DOCKER_COMPOSE_ENV_FILE=${DOCKER_COMPOSE_ENV_FILE/$CI_CD_PROJECT_PATH/$HOST_PROJECT_PATH}

    echo "Project Path: ${CI_CD_PROJECT_PATH}" 
    echo "Docker Compose File: ${DOCKER_COMPOSE_FILE}" 
    echo "Docker Folder Path: ${DOCKER_COMPOSE_PATH}" 
    echo "Host Project Path: ${HOST_PROJECT_PATH}" 
    echo "Host Docker Folder Path: ${HOST_DOCKER_COMPOSE_PATH}" 

    # change target to be testing instead of development
    if [[ "$OSTYPE" == "darwin"* ]]; then
    # MacOS
        echo "MacOS"
        sed -Ei '' 's/(.*=)(.*[[:space:]].*)/\1\"\2\"/' "${DOCKER_COMPOSE_ENV_FILE}"
        sed -Ei '' 's/target: development/target: testing/' "${DOCKER_COMPOSE_FILE}"
        sed -Ei '' 's/(image: .*)development/\1testing/' "${DOCKER_COMPOSE_FILE}"
    else
    # Linux
        echo "Linux"
        sed -Ei 's/(.*=)(.*[[:space:]].*)/\1\"\2\"/' "${DOCKER_COMPOSE_ENV_FILE}"
        sed -Ei 's/target: development/target: testing/' "${DOCKER_COMPOSE_FILE}"
        sed -Ei 's/(image: .*)development/\1testing/' "${DOCKER_COMPOSE_FILE}"
    fi


    # build the image, up it (so it creates all the services)
    PROJECT_ID=$( cat ${DOCKER_COMPOSE_ENV_FILE} | grep "PROJECT_ID" | awk -F "=" '{print $2}' )
    CONTAINER_NAME=$(eval "echo $(yq .services.${PROJECT_ID}.container_name ${DOCKER_COMPOSE_FILE} | sed 's/"//g')")
    PROJECT_PATH=${HOST_PROJECT_PATH} docker-compose -f "${DOCKER_COMPOSE_FILE}" --env-file ${DOCKER_COMPOSE_ENV_FILE} build 
    # PROJECT_PATH=${HOST_PROJECT_PATH} docker-compose -f "${DOCKER_COMPOSE_FILE}" --env-file ${DOCKER_COMPOSE_ENV_FILE} up -d
    # # then kill the service for our application
    # docker kill ${CONTAINER_NAME} && docker rm ${CONTAINER_NAME}
    # and create and start a container mounting extra volumes and passing extra env var (docker-compose up can't do this :/)
    echo "PROJECT_PATH=${HOST_PROJECT_PATH} docker-compose -f \"${DOCKER_COMPOSE_FILE}\" --env-file ${DOCKER_COMPOSE_ENV_FILE} run --name ${CONTAINER_NAME} --service-ports -d $(generate_inline_env) -e PROJECT_PATH=${HOST_PROJECT_PATH} -v \"${HOST_CI_CD_FOLDER}/commands\":\"/home/commands\" ${TTY} ${PROJECT_ID} bash -c -x \"tail -f /dev/null\""
    PROJECT_PATH=${HOST_PROJECT_PATH} docker-compose -f "${DOCKER_COMPOSE_FILE}" --env-file ${DOCKER_COMPOSE_ENV_FILE} run --name ${CONTAINER_NAME} --service-ports -d $(generate_inline_env) -e PROJECT_PATH=${HOST_PROJECT_PATH} -v "${HOST_CI_CD_FOLDER}/commands":"/home/commands" ${TTY} ${PROJECT_ID} bash -c -x "tail -f /dev/null"

    # loop throught the commands and execute them in the container
    for COMMANDS_ARRAY in "${COMMANDS_ARRAY[@]}"
    do 
        # get the array and process it
        mapfile -t COMMAND_ARRAY <<< $(jq -rc '.[]' <<< "${COMMANDS_ARRAY}")
        COMMAND_ARGUMENTS=()
        for ((i = 0; i < ${#COMMAND_ARRAY[@]}; ++i))
        do
            if [ $i -eq 0 ]; then
                # check the command string execute the file named the same
                COMMAND_FILE=$(find "${SCRIPT_DIR}/commands" -name "${COMMAND_ARRAY[$i]}.sh")
                if [ -n ${COMMAND_FILE} ] && [ ! -f "${COMMAND_FILE}" ]; then
                    echo "ERROR: Command \"${COMMAND_ARRAY[$i]}.sh\" not found"
                    exit 1
                fi 
            else
                # COMMAND_ARGUMENTS+=$( docker_env_inline "${DOCKER_COMPOSE_ENV_FILE}" eval "echo \"${COMMAND_ARRAY[$i]}\"")
                COMMAND_ARGUMENTS+=$(eval "$( docker_env_inline "${DOCKER_COMPOSE_ENV_FILE}" ) && echo \"${COMMAND_ARRAY[$i]}\"")
            fi
        done

        echo ${COMMAND_ARGUMENTS[@]}

        ##############################################################
        echo "Running command ${COMMAND_FILE} ${COMMAND_ARGUMENTS[@]}"
        set +e
        # using docker exec so the container env and temp files outside of mounted volumes are kept between commands
        docker exec ${WORKING_DIR} ${CONTAINER_NAME} bash -xc "${COMMAND_FILE/"${SCRIPT_DIR}/commands"/"/home/commands"} ${COMMAND_ARGUMENTS[@]}"
        if [ ! $? -eq 0 ]
        then
            echo "Error executing the command ${COMMAND_FILE} ${COMMAND_ARGUMENTS[@]}"
            exit 1;
            PROJECT_PATH=${HOST_PROJECT_PATH} docker-compose -f "${DOCKER_COMPOSE_FILE}" --env-file ${DOCKER_COMPOSE_ENV_FILE} down
        fi
        set -e
        echo "[Done]"
        echo ""
        #############################################################
    done
    # stop the container we are done with all the commands
    PROJECT_PATH=${HOST_PROJECT_PATH} docker-compose -f "${DOCKER_COMPOSE_FILE}" --env-file ${DOCKER_COMPOSE_ENV_FILE} down
}

TTY=""
while [ $# -gt 0 ] 
do
    key="$1"
    case $key in
        --projects-root-dir)
            # get the project root dir from the container and create the path on the host as we will run sibling containers
            PROJECTS_ROOT_DIR=$2
            HOST_PROJETS_ROOT_DIR=$(readlink -m "${HOST_PATH}/${PROJECTS_ROOT_DIR}")
            shift # past argument
            shift # past value
        ;;
        --no-tty)
            TTY="-T"
            shift
        ;;
        *)    # unknown option
            echo "Invalid option $1"
            exit 1;
        ;;
    esac
done

# check we have all variables set otherwise throw an error
for var in PROJECTS_ROOT_DIR
do
    if [[ -z "${!var:-}" ]]; then
        # to lower case  
        var=${var,,}
        # replace - by _
        var=${var//_/-}
        echo "--${var} has not been set and is required"
        exit 1
    fi
done

# ALL OUR DEFINED FOLDERS
echo "SCRIPT_DIR: ${SCRIPT_DIR}"
echo "CI_CD_FOLDER: ${CI_CD_FOLDER}"
echo "HOST_CI_CD_FOLDER: ${HOST_CI_CD_FOLDER}"
echo "HOST_PATH: ${HOST_PATH}"
echo "PROJECTS_ROOT_DIR: ${PROJECTS_ROOT_DIR}"
echo "HOST_PROJETS_ROOT_DIR: ${HOST_PROJETS_ROOT_DIR}"

# check for requirements for all projects
echo "---- CHECKING FOR REQUIREMENTS ----"
for PROJECT in ${PROJECTS_ROOT_DIR}/**/ci.json; do # Whitespace-safe and recursive

    # check this is a file as it also returns the pattern when nothing is found
    [ -f "$PROJECT" ] || continue

    PROJECT=$(realpath "${CI_CD_FOLDER}/${PROJECT}")

    # find the dockercompose file 
    find_docker_compose ${PROJECT}
done 

unset PROJECT_ID
unset DEBIAN_VERSION
unset NODE_VERSION

if [ -f "${PROJECTS_ROOT_DIR}/ci.json" ]; then
    echo "---- PROCESSING ROOT SETUP SCRIPTS ----"[]
    run_project "${PROJECTS_ROOT_DIR}/ci.json" "setup"
fi

## find all the projects in the project folder
echo "---- PROCESSING PROJECT PRE SCRIPTS ----"
for PROJECT in ${PROJECTS_ROOT_DIR}/**/ci.json; do # Whitespace-safe and recursive
    [ -f "${PROJECT}" ] || continue
    run_project ${PROJECT} "pre"
done

if [ -f "${PROJECTS_ROOT_DIR}/ci.json" ]; then
    echo "---- PROCESSING ROOT PRE SCRIPTS ----"
    run_project "${PROJECTS_ROOT_DIR}/ci.json" "pre"
fi

if [ -f "${PROJECTS_ROOT_DIR}/ci.json" ]; then
    echo "---- PROCESSING ROOT SCRIPTS ----"
    run_project "${PROJECTS_ROOT_DIR}/ci.json"
fi

echo "---- PROCESSING PROJECTS SCRIPTS ----"
for PROJECT in ${PROJECTS_ROOT_DIR}/**/ci.json; do # Whitespace-safe and recursive
    [ -f "${PROJECT}" ] || continue
    run_project ${PROJECT}
done

if [ -f "${PROJECTS_ROOT_DIR}/ci.json" ]; then
    echo "---- PROCESSING ROOT SCRIPTS ----"
    run_project "${PROJECTS_ROOT_DIR}/ci.json" "post"
fi

echo "---- PROCESSING PROJECTS POST SCRIPTS ----"
for PROJECT in ${PROJECTS_ROOT_DIR}/**/ci.json; do # Whitespace-safe and recursive
    [ -f "${PROJECT}" ] || continue
    run_project ${PROJECT} "post"
done

if [ -f "ci.json" ]; then
    echo "---- PROCESSING ROOT TEARDOWN SCRIPTS ----"
    run_project "${PROJECTS_ROOT_DIR}/ci.json" "teardown"
fi
